%define version    1.0.0
%define release    2
%define debug_package %{nil}
%global _build_id_links none
%undefine __brp_mangle_shebangs

Name:           CSTC-hardwareCap
Summary:        CSTC-hardwareCap Compatibility Test Suite
Version:        %{version}
Release:        %{release}
Group:          Development/Tools
License:        Mulan PSL v2
URL:            https://gitee.com/openeuler/oec-hardware
Source0:        %{name}-%{version}.tar.bz2

Buildroot:      %{_tmppath}/%{name}-%{version}-root
BuildRequires:  gcc
Requires:       kernel-devel, kernel-headers, dmidecode, tar
Requires:       qperf, fio, memtester
Requires:       kernel >= 4
Requires:       python3

# server subpackage
%package server
Summary:        CSTC-hardwareCap Compatibility Test Server
Group:          Development/Tools
Requires:       python3, python3-devel, nginx, tar, qperf, psmisc

%description
CSTC-hardwareCap Compatibility Test Suite

%description server
CSTC-hardwareCap Compatibility Test Server

%prep
%setup -q -c

%build
[ "$RPM_BUILD_ROOT" != "/" ] && [ -d $RPM_BUILD_ROOT ] && rm -rf $RPM_BUILD_ROOT;
DESTDIR=$RPM_BUILD_ROOT VERSION_RELEASE=%{version} make

%install
DESTDIR=$RPM_BUILD_ROOT make install

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && [ -d $RPM_BUILD_ROOT ] && rm -rf $RPM_BUILD_ROOT;

%pre

%post

%files
%defattr(-,root,root)
/usr/bin/cstchc
/usr/share/cstchc/kernelrelease.json
/usr/share/cstchc/lib/cstchccompatible
/usr/share/cstchc/lib/tests
/usr/lib/systemd/system/cstchc.service
%dir /var/cstchc
%dir /usr/share/cstchc/lib
%dir /usr/share/cstchc

%files server
%defattr(-,root,root)
/usr/share/cstchc/lib/server
/usr/share/cstchc/lib/server/uwsgi.ini
/usr/share/cstchc/lib/server/uwsgi.conf
/usr/lib/systemd/system/cstchc-server.service

%postun
rm -rf /var/lock/cstchc.lock

%changelog
* Sun Jul 01 2020 Cui XuCui <cuixucui@huawei.com> - 1.0.0-2
* Fri Jul 26 2019 Lu Tianxiong <lutianxiong@huawei.com> - 1.0.0-h1
- Initial spec

