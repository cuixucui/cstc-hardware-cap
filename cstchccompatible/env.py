#!/usr/bin/env python
# coding: utf-8

# Copyright (c) 2020 CSTCHC Technologies Co., Ltd.
# cstc-hardware-cap is licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Create: 2020-04-01

"""Certification file path"""


class CertEnv:
    """
    Certification file path
    """
    environmentfile = "/etc/cstchc.json"
    releasefile = "/etc/os-release"
    datadirectory = "/var/cstchc"
    certificationfile = datadirectory + "/compatibility.json"
    devicefile = datadirectory + "/device.json"
    factoryfile = datadirectory + "/factory.json"
    rebootfile = datadirectory + "/reboot.json"
    testdirectoy = "/usr/share/cstchc/lib/tests"
    logdirectoy = "/usr/share/cstchc/logs"
    resultdirectoy = "/usr/share/cstchc/lib/server/results"
    kernelinfo = "/usr/share/cstchc/kernelrelease.json"
