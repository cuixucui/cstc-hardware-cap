#!/usr/bin/env python
# coding: utf-8

# Copyright (c) 2020 CSTCHC Technologies Co., Ltd.
# cstc-hardware-cap is licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Create: 2020-04-01

"""Test Non-Volatile Memory express"""

import os
import sys
import random
import string
import argparse
from cstchccompatible.test import Test
from cstchccompatible.command import Command
from cstchccompatible.device import CertDevice

class NvmeTest(Test):
    """
    Test Non-Volatile Memory express
    """
    def __init__(self):
        Test.__init__(self)
        self.requirements = ["nvme-cli"]
        self.args = None
        self.device = None

    def setup(self, args=None):
        """
        Initialization before test
        :param args:
        :return:
        """
        self.args = args or argparse.Namespace()
        self.device = getattr(args, 'device', None)
        self.pci = self.device.get_pci()
        self.device = str(CertDevice.port_devices[self.pci])
        self.device = self.device.strip("[]")
        Command("nvme list").echo(ignore_errors=True)

    def test(self):
        """
        test case
        :return:
        """
        print("Vendor Info:")
        Command("lspci -s %s -v" % self.pci).echo()
        
        print("Driver Info:")
        Command("modinfo nvme| head -n 10").echo()
        
        disk = str(self.device)
        if self.in_use(disk):
            print("%s is in use now, skip this test." % disk)
            return False

        size = Command("cat /sys/block/%s/size" % disk).get_str()
        size = int(int(size))/2/2
        if size <= 0:
            print("Error: the size of %s is not suitable for this test." % disk)
            return False
        elif size > 10*1024*1014*1024:
            size = 10*1024*1014*1024

        try:
            print("\n##Formatting...")
            if not os.system("nvme list|grep %s|grep INTEL" % disk):
                Command("mkfs -t ext4  /dev/%s &> /dev/null" % disk).echo()
                sys.stdout.flush()
            else:
                Command("nvme format -l 0 -i 0 /dev/%s &> /dev/null" % disk).echo()
                sys.stdout.flush()
            print("Format: Success")
       
            print("\n##Writting...")
            ran_str = ''.join(random.sample(string.ascii_letters + string.digits, 10))
            Command("echo %s | nvme write /dev/%s --data-size=520 --prinfo=1" \
                   % (ran_str, disk)).echo()
            sys.stdout.flush()

            print("\n##Reading...")
            
            Command("nvme read /dev/%s --data-size=520 --prinfo=1 > /opt/readValue" % disk).echo()
            sys.stdout.flush()
            with open("/opt/readValue") as value:   
                if value.readline().strip() == ran_str:
                    print("\n##The written content is the same as the read content.")
                else:
                    print("\n##The written content is not the same as the read content.")
            
            print("\n##Smart Log:")
            Command("nvme smart-log /dev/%s 2> /dev/null" % disk).echo()
            sys.stdout.flush()

            print("\n##Log:")
            Command("nvme get-log -i 1 -l 128 /dev/%s 2> /dev/null" % disk).echo()
            print("")
            sys.stdout.flush()

            Command("nvme list").echo(ignore_errors=True)
            return True
        except Exception as concrete_error:
            print("Error: nvme cmd fail.")
            print(concrete_error)
            return False

    def in_use(self, disk):
        """
        Determine whether the swapon is in use
        :param disk:
        :return:
        """
        os.system("swapon -a 2>/dev/null")
        
        swap_file = open("/proc/swaps", "r")
        swap = swap_file.read()
        swap_file.close()

        mdstat_file = open("/proc/mdstat", "r")
        mdstat = mdstat_file.read()
        mdstat_file.close()

        mtab_file = open("/etc/mtab", "r")
        mtab = mtab_file.read()
        mtab_file.close()

        mount_file = open("/proc/mounts", "r")
        mounts = mount_file.read()
        mount_file.close()

        if ("/dev/%s" % disk) in swap or disk in mdstat:
            return True
        if ("/dev/%s" % disk) in mounts or ("/dev/%s" % disk) in mtab:
            return True
        if os.system("pvs 2>/dev/null | grep -q '/dev/%s'" % disk) == 0:
            return True
